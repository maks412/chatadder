package com.example.mgaliyev.chatapp;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

public class MainActivity extends AppCompatActivity {
    ListView lv;
    Context context;

    DBHandler dbHandler;

    ArrayList<String> texts = new ArrayList();
    ArrayList times = new ArrayList();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final TextView text = findViewById(R.id.text);
        Button send = findViewById(R.id.send);

        context=this;
        lv=(ListView) findViewById(R.id.list);

        dbHandler = new DBHandler(MainActivity.this);

        texts = dbHandler.getAllShops();
        times = dbHandler.getAllTimes();

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT+1:00"));
                Date currentLocalTime = cal.getTime();
                DateFormat date = new SimpleDateFormat("HH:mm a");
                date.setTimeZone(TimeZone.getTimeZone("GMT+6:00"));
                String localTime = date.format(currentLocalTime);

                dbHandler = new DBHandler(MainActivity.this);
                ListModel lm = new ListModel();
                lm.setText(text.getText().toString());
                lm.setTime(localTime.toString());
                dbHandler.addShop(lm);

                texts = dbHandler.getAllShops();
                times = dbHandler.getAllTimes();

                lv.setAdapter(new ListAdapter(MainActivity.this, texts,times));

            }
        });

        send.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                dbHandler.Delete();
                return false;
            }
        });

        lv.setAdapter(new ListAdapter(MainActivity.this, texts,times));


    }

}

package com.example.mgaliyev.chatapp;

        import android.content.Context;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.View.OnClickListener;
        import android.view.ViewGroup;
        import android.widget.BaseAdapter;
        import android.widget.EditText;
        import android.widget.TextView;
        import android.widget.Toast;

        import java.util.ArrayList;

public class ListAdapter extends BaseAdapter{
    ArrayList result;
    Context context;
    ArrayList imageId;
    private static LayoutInflater inflater=null;
    public ListAdapter(MainActivity mainActivity, ArrayList prgmNameList, ArrayList prgmImages) {
        // TODO Auto-generated constructor stub
        result=prgmNameList;
        context=mainActivity;
        imageId=prgmImages;
        inflater = ( LayoutInflater )context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return result.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View rowView;
        rowView = inflater.inflate(R.layout.list_item, null);
        EditText text =(EditText) rowView.findViewById(R.id.text_item);
        TextView time =(TextView) rowView.findViewById(R.id.time_item);
        text.setText(result.get(position).toString());
        time.setText(imageId.get(position).toString());
        rowView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Toast.makeText(context, "You Clicked "+result.get(position).toString(), Toast.LENGTH_LONG).show();
            }
        });
        return rowView;
    }

}
package com.example.mgaliyev.chatapp;

/**
 * Created by MGaliyev on 29.11.2017.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;


public class DBHandler extends SQLiteOpenHelper {


    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "chats";
    private static final String TABLE_SHOPS = "chat";
    private static final String KEY_ID = "id";
    private static final String KEY_TEXT = "text";
    private static final String KEY_TIME = "time";
    public DBHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_SHOPS + "("
        + KEY_TEXT + " TEXT,"
        + KEY_TIME + " TEXT" + ")";
        db.execSQL(CREATE_CONTACTS_TABLE);
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SHOPS);
        onCreate(db);
    }

    public void addShop(ListModel shop) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_TEXT, shop.getText()); // Shop Name
        values.put(KEY_TIME, shop.getTime()); // Shop Phone Number
        db.insert(TABLE_SHOPS, null, values);
        db.close();
    }

    public ArrayList<String> getAllShops() {
        ArrayList<String> shopList = new ArrayList<String>();
        String selectQuery = "SELECT * FROM " + TABLE_SHOPS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                String shop;
                shop = cursor.getString(0);
                shopList.add(shop);
            } while (cursor.moveToNext());
        }
        return shopList;
    }


    public ArrayList<String> getAllTimes() {
        ArrayList<String> shopList = new ArrayList<String>();
        String selectQuery = "SELECT * FROM " + TABLE_SHOPS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                String shop;
                shop = cursor.getString(1);
                shopList.add(shop);
            } while (cursor.moveToNext());
        }

        return shopList;
    }

    public void Delete(){
        String selectQuery = "DELETE FROM " + TABLE_SHOPS;
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(selectQuery);
        db.close();
    }
}

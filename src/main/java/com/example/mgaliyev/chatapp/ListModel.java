package com.example.mgaliyev.chatapp;

/**
 * Created by MGaliyev on 29.11.2017.
 */

public class ListModel {
    private String text;
    private  String time;

    public ListModel(){
        //this.text = text;
        //this.time = time;
    }
    public String getText() {
        return text;
    }

    public String getTime() {
        return time;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
